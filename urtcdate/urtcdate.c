#include "../config.h"
#include <urtc.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <time.h>
#include <assert.h>
#include <string.h>
#include <errno.h>
#include <syslog.h>

#define STRING_BUFF 128
#define TIMESTAMP_FORMAT "%m%d%H%M%Y.%S"

// ������� ����� ��������� �����
const static char* const short_options = "hsrc";
const static struct option long_options[] = {
	{"help",		 no_argument,		NULL, 'h'},
	{"update",		 optional_argument,	NULL, 'U'},
	{"reset",		 no_argument,		NULL, 'r'},
	{"sync",		 no_argument,		NULL, 's'},
	{"conf",		 no_argument,		NULL, 'c'},
	{NULL, 0, NULL, 0}
};

/* Help usage */
static inline void help_print(void) {
	printf("Urtctool ver. %s\n", VERSION);
	printf("Created by Vladimir Inshakov <markoni48@yandex.ru>, 2020.\n\
Usage:\n\n\
  urtctool [-h|-c|-s|-r|--update[=<timestamp>]]\n\n\
Keys:\n\
  -h, --help              This message.\n\n\
  --update[=<timestamp>]  Set default RTC timestamp.\n\
                          The <timestamp> format is MMDDhhmmYYYY.ss\n\
                          for example: 090819172020.04\n\
                          If timestamp is none, then will be set system\n\
                          local time and date.\n\n\
  -s, --sync              Set current system time and date from RTC.\n\
                          (It's only for root user)\n\n\
  -r, --reset             Reset RTC control registers to default.\n\n\
  -c, --conf              Print default configuration.\n");
}

/* ��������� ������ �������� �������� ��������� ����� */
static inline void print_local_timestamp(void) {

	time_t now = time(NULL);						// Get the current time.

	char *curTime_s = ctime(&now);
	assert(curTime_s[0] != 0);
	printf("Current local time: %s", curTime_s);
}

/* �������� ����������� � �������� ���������� RTC */
static URTC_h createNopen_rtc_device(void) {

	/* ���� ���������� ������� � ���������� */
	URTC_h urtc_h = createDefaultRtc();
	if (!urtc_h) {
		syslog(LOG_ERR, "Error create default RTC device descriptor.");
		errno = EFAULT;
		return NULL;
	}

	int result = open_urtc(urtc_h);
	if (result) {
		syslog(LOG_ERR, "Cannot open bus device '%s': %s.", getDefaultBusDevicePath(), strerror(errno));
		goto err_exit;
	}

	return urtc_h;

err_exit:
	destroyRtcDevice(urtc_h);
	return NULL;
}

/* ��������� ������ �������� �������� ����� � ������� */
static inline int print_rtc_timestamp(void) {

	URTC_h urtc_h = createNopen_rtc_device();
	if (!urtc_h) return EXIT_FAILURE;

	struct tm timestamp;
	int result = urtc_load_timestamp(urtc_h, &timestamp);
	if (result) {
		fprintf(stderr, "Error load RTC-chip %s timestamp: %s.", getDefaultChipName(), strerror(errno));
		goto err_exit;
	}

	char buff[STRING_BUFF]; memset(buff, 0, sizeof(buff));
	printf("Current RTC timestamp: %s", asctime_r(&timestamp, buff));

	result = EXIT_SUCCESS;

err_exit:
	close_urtc(urtc_h);
	destroyRtcDevice(urtc_h);
	return result;
}

static int defaultRun(void) {

	print_local_timestamp();
	return print_rtc_timestamp();
}

/* ���������� ������� ������� � ���������� RTC.
*  str2parse = NULL - ��������������� ��������� �����
*  str2parse = <timestamp_string_to_parse>, ������������ ����� � ������� TIMESTAMP_FORMAT
*/
static int updateRtc(const char* str2parse, const char* tpl) {

	struct tm b_tm;

	// User date
	if (str2parse && str2parse[0]) {
		memset(&b_tm, 0, sizeof(b_tm));

		char *time_str = strptime(str2parse, tpl, &b_tm);
		if (!time_str) {
			syslog(LOG_ERR, "Error parse timestamp string \"%s\": %s.", str2parse, strerror(EINVAL));
			return EINVAL;
		}
		
		assert(time_str != NULL);
	}
	// Default NOW
	else {
		time_t timestamp = time(NULL);
		localtime_r(&timestamp, &b_tm);
	}

	/* ���� ���������� ������� � ���������� */
	URTC_h urtc_h = createNopen_rtc_device();
	if (!urtc_h) return EXIT_FAILURE;

	int result = open_urtc(urtc_h);
	if (result) {
		syslog(LOG_ERR, "Error open bus device '%s': %s.", getDefaultBusDevicePath(), strerror(errno));
		goto err_exit;
	}

	result = urtc_save_timestamp(urtc_h, &b_tm);
	if (result) {
		syslog(LOG_ERR, "Error set RTC-chip %s timestamp: %s.", getDefaultChipName(), strerror(errno));
		goto err_exit;
	}

	syslog(LOG_INFO, "Update uRTC timestamp '%s'.", asctime(&b_tm));

err_exit:
	close_urtc(urtc_h);

	destroyRtcDevice(urtc_h);
	return result;
}

/* ��������� ������������� ��������� ����� ���������� �� ��������� �� ���� RTC */
static inline int syncSystemCalendar(void) {

	URTC_h urtc_h = createNopen_rtc_device();
	if (!urtc_h) return EXIT_FAILURE;

	struct tm bd_tm;
	int result = urtc_load_timestamp(urtc_h, &bd_tm);
	if (result) {
		syslog(LOG_ERR, "Error load RTC timestamp: %s.", strerror(errno));
		goto err_exit;
	}
	time_t timestamp = mktime(&bd_tm);
	assert(timestamp > 0);

	/* Set local time */
	result = stime(&timestamp);
	if (result) syslog(LOG_ERR, "Error change system local timestamp: %s.", strerror(errno));

	syslog(LOG_INFO, "Sync current timestamp '%s' from uRTC.", asctime(&bd_tm));

err_exit:
	result = close_urtc(urtc_h);

	destroyRtcDevice(urtc_h);
	return result;
}

/* ��������� ������ ���������� ��������� � 0 */
static inline int doResetDefaultRTC(void) {

	URTC_h urtc_h = createNopen_rtc_device();
	if (!urtc_h) return EXIT_FAILURE;

	int result = urtc_reset_default(urtc_h);

	printf("Reset RTC configuration to default successful done.\n");

	result = close_urtc(urtc_h);
	destroyRtcDevice(urtc_h);

	return result;
}


// ���������� ��������� ������������
static inline void print_default_parameters(void) {
	printf("liburtc configuration\n");
	printf("------------------------\n");
	printf("default device bus path: %s\n", getDefaultBusDevicePath());
	printf("       default RTC chip: %s\n", getDefaultChipName());
}


int main(int argc, char* argv[]) {

	openlog(NULL, LOG_PERROR | LOG_NDELAY, LOG_MAKEPRI(LOG_USER, LOG_ERR));

	// ������� ������
	int nextOpt, result;
	do {
		nextOpt = getopt_long(argc, argv, short_options, long_options, NULL);

		switch (nextOpt)
		{
		case 'h':
		case '?':
			help_print();
			result = EXIT_SUCCESS;
			goto endprog;
		case 'c':
			print_default_parameters();
			result = EXIT_SUCCESS;
			goto endprog;
		case 's':			// --sync option
			result = syncSystemCalendar();
			goto endprog;
		case 'r':			// --reset option
			result = doResetDefaultRTC();
			goto endprog;

		/* Update RTC timestamp option */
		case 'U':			// --update option			
			result = updateRtc(optarg, TIMESTAMP_FORMAT);
			goto endprog;
		}
	} while (nextOpt != -1);

	return defaultRun();  // Run default mode (no-options)

endprog:
	closelog();
	return result;
}
