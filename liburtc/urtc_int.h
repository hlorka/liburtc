#ifndef URTCINT_H
#define URTCINT_H 1

#include "../config.h"
#include "urtc.h"

#ifndef LIBCONFDIR
#error You must specify LIBCONFDIR directive.
#endif
#define LIBCONFIG_FILENAME LIBCONFDIR "/liburtc.conf"
#define TMP_PROPERTY_LINE_SIZE 128
#define TMP_TEXT_BUFF_SIZE 1024

// ������ ��� ����� �����������
typedef int(*fopen_dev_t)(chipDev_t*, const char*);
typedef int(*fclose_dev_t)(chipDev_t*);

/* ���� ����� ������� ��� ��������� RTC*/
typedef int(*fmisc_t)(const chipDev_t*);
typedef int(*fload_timestamp_t)(const chipDev_t*, struct tm*);
typedef int(*fsave_timestamp_t)(const chipDev_t*, const struct tm*);

#endif
