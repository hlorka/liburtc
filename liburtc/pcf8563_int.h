#ifndef PCF8563_INT_H
#define PCF8563_INT_H 1

#include "urtc_int.h"

int pcf8563_load_timestamp(const chipDev_t*__restrict pDev, struct tm*__restrict pTm);
int pcf8563_save_timestamp(const chipDev_t*__restrict pDev, const struct tm*__restrict pTm);

// ��������� ����������� ��������� ��-���������
int pcf8563_reset(const chipDev_t*__restrict pDev);

#endif /* !PCF8563_INT_H */
