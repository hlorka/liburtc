#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <assert.h>
#include "urtc_int.h"
#include "helpers.h"
#include "pcf8563.h"
#include "pcf8563_int.h"


static struct {
	unsigned isLoaded:1;
	char* def_bPath;
	char* def_ChipName;
} default_config = {
	.isLoaded = 0,
	.def_bPath = NULL,
	.def_ChipName = NULL
};

struct urtc_h {
	chipDev_t chipDev;
	char* bus_dev_path;
	fopen_dev_t open_dev_f;
	fclose_dev_t close_dev_f;
	fload_timestamp_t load_timestamp_f;
	fsave_timestamp_t save_timestamp_f;
	fmisc_t start_f;
	fmisc_t stop_f;
	fmisc_t reset_f;
};


unsigned int getLibVersion (void)
{
	// TODO: �������� �������� ����������� ������ � ������� BCD
	return 0x0100;
}


static inline void load_default_config(void) {

	if (default_config.isLoaded) return;

	default_config.isLoaded = 0;

	int result;
	char text_buff[TMP_TEXT_BUFF_SIZE];
	if (load_textfile(LIBCONFIG_FILENAME, text_buff, sizeof(text_buff))) return;

	size_t len;
	char tmpStr[TMP_PROPERTY_LINE_SIZE];

	// �������� ��������� ���� ���� ����������
	parse_property(text_buff, "dev-path", tmpStr, sizeof(tmpStr));
	len = strlen(tmpStr);
	if (len) {
		default_config.def_bPath = malloc(len);
		if (!default_config.def_bPath) {
			errno = ENOMEM;
			return;
		}
		strcpy(default_config.def_bPath, tmpStr);
	}
	assert(default_config.def_bPath != NULL);

	// �������� ���������� ��-���������
	parse_property(text_buff, "default-chipname", tmpStr, sizeof(tmpStr));
	len = strlen(tmpStr);
	if (len) {
		default_config.def_ChipName = malloc(len);
		if (!default_config.def_ChipName) goto exit_error;
		strcpy(default_config.def_ChipName, tmpStr);
	}
	assert(default_config.def_ChipName != NULL);

	default_config.isLoaded = 1;

	return;

exit_error:
	errno = ENOMEM;
	free(default_config.def_bPath);
}


const char* getDefaultChipName (void) {
	assert(default_config.def_ChipName);
	return default_config.def_ChipName;
}

const char* getDefaultBusDevicePath(void) {
	assert(default_config.def_bPath);
	return default_config.def_bPath;
}

chipDev_t* getChipDeviceDesc (URTC_h __restrict hDev) {
	assert(hDev);
	return &((struct urtc_h*) hDev)->chipDev;
}

URTC_h createCustomRtcDevice (const char*__restrict bus_dev_path, unsigned int rtcdev_addr) {

	assert(bus_dev_path != NULL);

	struct urtc_h* pUrtc_h = (struct urtc_h*) malloc(sizeof(struct urtc_h));
	if (pUrtc_h == NULL) goto err_exit1;
	memset(pUrtc_h, 0, sizeof(struct urtc_h));

	size_t p_length = strlen(bus_dev_path);
	if (p_length < 5 || p_length > 100) goto err_exit2;

	pUrtc_h->bus_dev_path = (char*) malloc(p_length + 1);
	if (pUrtc_h->bus_dev_path == NULL) goto err_exit2;
	memset(pUrtc_h->bus_dev_path, 0, p_length + 1);
	
	strcpy(pUrtc_h->bus_dev_path, bus_dev_path);

	// Deleting 'const'- modifier.
	uint32_t* pUnconst = (void*) &pUrtc_h->chipDev + sizeof(int);

	assert(bus_dev_path > 0);
	*pUnconst = rtcdev_addr;

	return pUrtc_h;

err_exit2:
	free(pUrtc_h);
	pUrtc_h = NULL;

err_exit1:
	return pUrtc_h;
}

URTC_h createRtcByName(const char*__restrict rtc_name) {

	struct urtc_h* pDev = NULL;

	if (!default_config.isLoaded) goto end_func;

	assert(rtc_name != NULL);

	if (strcasecmp(rtc_name, PCF8563_CHIPNAME) == 0) {

		pDev = (struct urtc_h*) createCustomRtcDevice(default_config.def_bPath, PCF8563_I2C_BUS_ADDR);
		if (pDev == NULL) goto end_func;

		pDev->open_dev_f = open_i2c_device;
		pDev->close_dev_f = close_i2c;
		pDev->load_timestamp_f = pcf8563_load_timestamp;
		pDev->save_timestamp_f = pcf8563_save_timestamp;
		pDev->reset_f = pcf8563_reset;
	}
	else {
		errno = EINVAL;
	}

end_func:
	return pDev;
}


URTC_h createDefaultRtc(void) {

	if (default_config.isLoaded) return createRtcByName(default_config.def_ChipName);

	return NULL;
}

void destroyRtcDevice(URTC_h hDev) {
	if (hDev == NULL) return;

	struct urtc_h* pDev = (struct urtc_h*) hDev;

	/* �������� ��������� ��������� ����������� */
	if (pDev->chipDev.bus_fd != 0) close_urtc(hDev);

	free(pDev->bus_dev_path);
	free(pDev);

	hDev = NULL;
}


int open_urtc(URTC_h __restrict hDev) {

	struct urtc_h* pDev = (struct urtc_h*) hDev;

	if (hDev == NULL || pDev->open_dev_f == NULL) {
		errno = EINVAL;
		return -EINVAL;
	}

	return pDev->open_dev_f(&pDev->chipDev, pDev->bus_dev_path);
}


int urtc_reset_default(const URTC_h hDev)
{
	struct urtc_h* pDev = (struct urtc_h*) hDev;

	int result;

	if (hDev == NULL || pDev->open_dev_f == NULL) {
		errno = EINVAL;
		return -EINVAL;
	}

	if (pDev->reset_f) {
		return pDev->reset_f(&pDev->chipDev);
	}

	errno = ENOTSUP;
	return -ENOTSUP;
}


int close_urtc(URTC_h __restrict hDev) {

	struct urtc_h* pDev = (struct urtc_h*) hDev;
	if (hDev == NULL || pDev->close_dev_f == NULL) return -EINVAL;

	return pDev->close_dev_f(&pDev->chipDev);
}


int urtc_load_timestamp (const URTC_h hDev, struct tm*__restrict pTm) {
	if (hDev == NULL) {
		errno = EINVAL;
		return -EINVAL;
	}

	struct urtc_h* pDev = (struct urtc_h*) hDev;

	if (pDev->load_timestamp_f == NULL) {
		errno = ENOTSUP;
		return -ENOTSUP;
	}

	return pDev->load_timestamp_f(&pDev->chipDev, pTm);
}


int urtc_save_timestamp(const URTC_h hDev, const struct tm*__restrict pTm) {
	if (hDev == NULL) {
		errno = EINVAL;
		return -EINVAL;
	}

	struct urtc_h* pDev = (struct urtc_h*) hDev;

	if (pDev->save_timestamp_f == NULL) {
		errno = ENOTSUP;
		return -ENOTSUP;
	}

	return pDev->save_timestamp_f(&pDev->chipDev, pTm);
}

/* ������� 'init' (�������� ����������) */
void __attribute__((constructor))
dll_load(void)
{
	load_default_config();
}


/* ������� 'unload' (�������� ����������) */
void __attribute__((destructor))
dll_unload(void)
{
	/* Unload default config */
	if (default_config.isLoaded) {
		if (default_config.def_bPath) free(default_config.def_bPath);
		if (default_config.def_ChipName) free(default_config.def_ChipName);
		default_config.isLoaded = 0;
	}
}

#undef TMP_PROPERTY_LINE_SIZE
#undef TMP_TEXT_BUFF_SIZE