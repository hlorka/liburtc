#ifndef HELPERS_H
#define HELPERS_H 1

#include <stdint.h>
#include <string.h>

/* ����������� ������� BCD � ���������� ����� */
uint8_t bcd2dec(uint8_t bcd);

/* ����������� ����������� ������� � BCD */
uint8_t dec2bcd(uint8_t dec);

/* ��������� ��������� ���� � ������ */
int load_textfile(const char*__restrict filename, char* pText, size_t text_buff_size);

/* ��������� ������ �������� ���������� ��������� */
void parse_property(const char*__restrict filetext, const char * __restrict property_name, char* value, size_t val_string_size);

#endif /* !HELPERS_H */
