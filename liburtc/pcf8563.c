#include "pcf8563.h"
#include "pcf8563_int.h"
#include "helpers.h"
#include <string.h>

#define CENTURY_YEARS 100

int pcf8563_load_timestamp(const chipDev_t*__restrict pDev, struct tm*__restrict pTm) {
	
	uint8_t tm_register[7];

	int result = i2c_read_array(pDev, PCF8563_REG_VL_SECONDS, tm_register, sizeof(tm_register) / sizeof(tm_register[0]));
	if (result) return result;

	pTm->tm_sec = bcd2dec(tm_register[0] & 0x7F);
	pTm->tm_min = bcd2dec(tm_register[1] & 0x7F);
	pTm->tm_hour = bcd2dec(tm_register[2] & 0x3F);
	pTm->tm_mday = bcd2dec(tm_register[3] & 0x3F);
	pTm->tm_wday = tm_register[4] & 0x07;
	pTm->tm_mon = bcd2dec(tm_register[5] & 0x1F) - 1;
	pTm->tm_year = bcd2dec(tm_register[6]) + CENTURY_YEARS;

	return result;
}


int pcf8563_save_timestamp(const chipDev_t*__restrict pDev, const struct tm*__restrict pTm) {

	uint8_t tm_register[7];

	tm_register[0] = dec2bcd(pTm->tm_sec) & 0x7F;
	tm_register[1] = dec2bcd(pTm->tm_min) & 0x7F;
	tm_register[2] = dec2bcd(pTm->tm_hour) & 0x3F;
	tm_register[3] = dec2bcd(pTm->tm_mday) & 0x3F;
	tm_register[4] = pTm->tm_wday & 0x07;
	tm_register[5] = dec2bcd(pTm->tm_mon + 1) & 0x1F;
	tm_register[6] = dec2bcd(pTm->tm_year - CENTURY_YEARS);

	int result = i2c_update_array(pDev, PCF8563_REG_VL_SECONDS, tm_register, sizeof(tm_register) / sizeof(tm_register[0]));

	return result;
}

int pcf8563_reset(const chipDev_t*__restrict pDev)
{
	uint8_t pcf_ctr_register[2];
	memset(pcf_ctr_register, 0, sizeof(pcf_ctr_register));

	pcf8563_ctrls1_reg ctrl1 = {
		.bits.TEST1 = 0,
		.bits.STOP = 0,
		.bits.TESTC = 0
	};
	pcf_ctr_register[0] = ctrl1.data;

	pcf8563_ctrls2_reg ctrl2 = {
		.bits.TI_TP = 0,
		.bits.AF = 0,
		.bits.TF = 0,
		.bits.AIE = 0,
		.bits.TIE = 0
	};

	pcf_ctr_register[1] = ctrl2.data;

	return i2c_write_array(pDev, PCF8563_REG_CTRL_STATUS_1, pcf_ctr_register, sizeof(pcf_ctr_register) / sizeof(pcf_ctr_register[0]));
}

#undef CENTURY_YEARS 