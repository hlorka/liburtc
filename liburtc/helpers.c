#include "helpers.h"
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

#define TMPSTR_BUFF_SIZE 256


uint8_t bcd2dec(uint8_t bcd) {
	return (((bcd >> 4) & 0x0F) * 10) + (bcd & 0x0F);
}

uint8_t dec2bcd(uint8_t dec) {
	return (dec / 10) * 16 + dec % 10;
}

int load_textfile(const char*__restrict filename, char* pText, size_t text_buff_size)
{
	int result = EXIT_SUCCESS;
	FILE* cfg_fd;
	cfg_fd = fopen(filename, "r");
	if (cfg_fd == NULL) return errno;

	char tmp_str[TMPSTR_BUFF_SIZE], *textline;

	memset(pText, 0, text_buff_size);
	char* offset = pText;

	size_t sl;
	memset(tmp_str, 0, sizeof(tmp_str));
	while (textline = fgets(tmp_str, sizeof(tmp_str) - 1, cfg_fd)) {
		if (textline[0] == '\n' || textline[0] == '#' || textline[0] == '/' || textline[0] == ';') continue;	// �������� �� �����������

		sl = strlen(textline);
		if ((pText + text_buff_size) < (offset + sl)) break;	// �������� ������ ������

		strcpy(offset, textline);
		offset += sl;

		memset(tmp_str, 0, sizeof(tmp_str));
	}

	fclose(cfg_fd);
	return result;
}


void parse_property(const char*__restrict filetext, const char * __restrict property_name, char* value, size_t val_string_size)
{
	const char delimiters[] = " =:\n";

	size_t sl = strlen(filetext);
	if (sl < 5) {
		errno = EINVAL;
		return;
	}

	char* tmpStr = malloc(++sl);
	if (tmpStr == NULL) {
		errno = ENOMEM;
		return;
	}
	strcpy(tmpStr, filetext);

	memset(value, 0, val_string_size);
	char *token = strtok(tmpStr, delimiters);			// ������ ������� ����

	do {
		// ����� ���������
		if (strcasecmp(token, property_name) == 0) {
			// �������� ��������
			if (token = strtok(NULL, delimiters)) {
				strncpy(value, token, val_string_size);
			}
			break;
		}
	} while (token = strtok(NULL, delimiters));

	free(tmpStr);
}


