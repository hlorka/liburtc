#include "urtc.h"
#include <string.h>
#include <regex.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>

#define DEVICENAME_BUFF_SIZE 20


static int check_i2c_devpath(const char*__restrict path) {
	regex_t regex;
	int check_result = 0;

	/* Compile regular expression */
	int result = regcomp(&regex, "^\\/dev\\/[0-9a-zA-Z\\/]*i2c-[0-9]{1,2}$", REG_EXTENDED);
	if (result != EXIT_SUCCESS) return check_result;

	/* Execute regular expression */
	result = regexec(&regex, path, 0, NULL, 0);
	if (result == EXIT_SUCCESS) {
		check_result = 1;
	}
	else if (result == REG_NOMATCH) {
		check_result = 0;
	}

	/* Free memory allocated to the pattern buffer by regcomp() */
	regfree(&regex);
	return check_result;
}


static inline int i2c_bus_tune(int df) {

	int result;

	// ����� ������� ���������� � ���������� �� ������
	result = ioctl(df, I2C_RETRIES, 3);
	if (result) return result;

	// ������� �������� x10 ��
	result = ioctl(df, I2C_TIMEOUT, 10);
	if (result) return result;

	return EXIT_SUCCESS;
}


static int i2c_set_slave_chip_addr(const chipDev_t*__restrict pDev) {

	int result;

	if (pDev->addr <= 0xFF) {
		// set to 7-bit device addr
		result = ioctl(pDev->bus_fd, I2C_TENBIT, 0);
		}
	else {
		// set to 10-bit device addr
		result = ioctl(pDev->bus_fd, I2C_TENBIT, 1);
	}
	if (result) return result;

	result = ioctl(pDev->bus_fd, I2C_SLAVE, pDev->addr);

	return result;
}


int open_i2c_device(chipDev_t*__restrict pDev, const char*__restrict pDevPath) {
	
	int result;
	if (!check_i2c_devpath(pDevPath)) return ENXIO;

	pDev->bus_fd = open(pDevPath, O_RDWR);
	if (pDev->bus_fd <= 0) return pDev->bus_fd;

	// �������������� ��������� ���������
	result = i2c_bus_tune(pDev->bus_fd);
	if (result) goto error_exit;

	// Set device slave address
	result = i2c_set_slave_chip_addr(pDev);
	if (result) goto error_exit;

	return EXIT_SUCCESS;

error_exit:
	close_i2c(pDev);
	return result;
}

int close_i2c(chipDev_t*__restrict pDev)
{
	int result = close(pDev->bus_fd);
	pDev->bus_fd = 0;
	return result;
}


int i2c_read_array(const chipDev_t*__restrict pDev, uint32_t offset, uint8_t*__restrict pBuf, size_t buf_size)
{
	int result;

	// TODO: �������� ���������� �� fd ��� �������� �������� �� ioctrl � fd
	result = i2c_set_slave_chip_addr(pDev);
	if (result) return result;

	// ������� ������ ��������� �����
	uint8_t offset_tmp = (offset & 0xFF);
	result = write(pDev->bus_fd, &offset_tmp, 1);
	if (result != 1) return EIO;

	memset(pBuf, 0, buf_size);

	// ������ � �����
	result = read(pDev->bus_fd, pBuf, buf_size);
	if (result != buf_size) result = EIO;

	return EXIT_SUCCESS;
}

int i2c_write_array(const chipDev_t*__restrict pDev, uint32_t offset, const uint8_t*__restrict pBuf, size_t buf_size)
{
	size_t buf2_size = buf_size + 1;
	uint8_t* pBuf2 = malloc(buf2_size);
	if (pBuf == NULL) return ENOMEM;
	memcpy(pBuf2 + 1, pBuf, buf_size);
	pBuf2[0] = offset;

	int result;

	// TODO: �������� ���������� �� fd ��� �������� �������� �� ioctrl � fd
	result = i2c_set_slave_chip_addr(pDev);
	if (result) goto exit_error2;

	result = write(pDev->bus_fd, pBuf2, buf2_size);
	if (result != buf2_size) {
		result = EIO;
		goto exit_error2;
	}

	return EXIT_SUCCESS;
	
exit_error2:
	free(pBuf2);
	return result;
}

int i2c_update_array(const chipDev_t*__restrict pDev, uint32_t offset, uint8_t*__restrict pBuf, size_t buf_size)
{
	int result;

	// ������ ������ �� ��� �� �������
	uint8_t* pBuf_read = malloc(buf_size);
	if (pBuf_read == NULL) return ENOMEM;

	result = i2c_read_array(pDev, offset, pBuf_read, buf_size);
	usleep(3);		// �������� ��� ���������� 3��

	if (result != 0) goto exit1;

	uint32_t offset2; unsigned int s, j;
	for (unsigned int i = 0; i < buf_size; i++) {
		if (pBuf[i] != pBuf_read[i]) {
			offset2 = offset + i;
			for (s = 0, j = i; i < buf_size; i++, s++) if (pBuf[i] == pBuf_read[i]) break;
			result = i2c_write_array(pDev, offset2, pBuf + j, s);
			if (result) goto exit1;
		}
	}

	result = EXIT_SUCCESS;

exit1:
	free(pBuf_read);
	return result;
}

#undef DEVICENAME_BUFF_SIZE