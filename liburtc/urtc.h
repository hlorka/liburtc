/** \file
	\brief ������� ���� API ���������� 'liburtc.so' ��� ������ � ��������� RTC

	Created by Vladimir Inshakov <markoni48@yandex.ru>, 2020 ���.
*/

/* BEGIN_C_DECLS should be used at the beginning of your declarations,
so that C++ compilers don�t mangle their names. Use END_C_DECLS at
the end of C declarations. */

#undef BEGIN_C_DECLS
#undef END_C_DECLS
#ifdef __cplusplus
# define BEGIN_C_DECLS extern "C" {
# define END_C_DECLS }
#else
# define BEGIN_C_DECLS /* empty */
# define END_C_DECLS /* empty */
#endif

#ifndef LIBURTC_H
#define LIBURTC_H 1

#include <stdint.h>
#include <time.h>

/// ���������� ���� RTC ��� ������ �������
typedef void* URTC_h;

/// ���������� ���� ������� � ����� RTC ��� �������������� �������
typedef struct _rtcChip_h {
	int bus_fd;
	const uint32_t addr;
} chipDev_t;

BEGIN_C_DECLS

/** �������� ������ ����������
 *	\return ������ � ������� BCD
*/
unsigned int getLibVersion (void);

/** \brief �������� ����������� RTC ��-���������
 *	
 * ������� � ������������ ������ ���������� ��-���������. ��������� ��-��������� ������ � ���������������� ����� 'liburtc.conf'.
 * ���� ���������� ������ �� �����, ���������� ���������� ������� � ������� ���, ������ 'destroyRtcDevice'.
*/
URTC_h createDefaultRtc (void);

/** \brief �������� ����������� RTC �� ������� ���������� ��������� ��������� �������
 *
 * ������� � ������������ ������ ���������� ��� ���������� � ���������� ��������� ��������� �������.
 * \param[in] rtc_name �������� ��������� ������������� ���������� RTC. ��������: PCF8563 
 * ���� ���������� ������ �� �����, ���������� ���������� ������� � ������� ���, ������ 'destroyRtcDevice'.
*/
URTC_h createRtcByName (const char*__restrict rtc_name);

/** \brief �������� ����������� RTC ��� ����������� ���������� ��������� �������
 *
 * ������� � ������������ ������ ���������� ��� �������� ���������� ����� ��������� �������.
 * \param[in] rtc_name ���� � �����-�������� (user-space) ���� � ������� ���������� ���������� (�������� "/dev/i2c-0")
 * \param[in] rtcdev_addr ����� ���������� (RTC) �� ����
 * ���� ���������� ������ �� �����, ���������� ���������� ������� � ������� ���, ������ 'destroyRtcDevice'.
*/
URTC_h createCustomRtcDevice (const char*__restrict bus_dev_path, unsigned int rtcdev_addr);

/** ���������� �������� ���� RTC ��-���������, ����������� � ���������������� �����.
 *
 * \return C-������ � ��������� ���������� RTC ��-���������.
*/
const char* getDefaultChipName (void);

/** ������ ���� � ����� ���������� ����, ����������� � ���������������� �����.
 *
 * \return C-������ ���� �� ����������������� �����.
*/
const char* getDefaultBusDevicePath (void);

/** \brief ������� ���������� RTC ��� ������������ ������ �������
 *
 * ��������� ���� �������� ���� ��� ������������ ������ ������� � ����������� RTC. ������� ����� �������� ������ �� ������ ����� ��: TODO:
 * \param[in,out] hDev ��������� �� ���������� ����� ��������� ����� �� ������� TODO: ...
 * ���������� ��������� ��� �������: ...
 * ���� ���������� ������ �� �����, ���������� ���������� ������� � ������� ���, ������ 'destroyRtcDevice'.
 * \return � ������ ������ ���������� ����� ��������� ����������� Linux ��� ��� ������, ���� �������� ����� 0.
 *		��� EINVAL, ���� hDev = NULL, ��������� ���� ���������� ��� Linux. 
*/
int open_urtc (URTC_h __restrict hDev);

/** \brief ������� ���� � ����� �� ����������
 *
 * ��������� ����� �� ���������� � ����������� ������� ���������� ��. ���������� ������ ���� ������� ������� ��. TODO:
 * \param[in] hDev ��������� �� ���������� ����� ��������� ����� �� ������� TODO: ...
 * \param[out] pTm ��������� �� ��������� ������ �������
 * \return 0 � ����� ������ ��� ��� ������.
 *		������ -ENOTSUP ��������, ���������� �� ������������ ������ ����������.
*/
int urtc_load_timestamp (const URTC_h hDev, struct tm*__restrict pTm);

/** \brief �������� ����� ���� � ����� � ����������
 *
 * ��������� (����������) ����� � ����������. ��� ����������, �������� ��������� ������ ��, ��������, ������� ����������
 * �� ��������� � ��������� ���������.
 *
 * \param[in] hDev ��������� �� ���������� ����� ��������� ����� �� ������� TODO: ...
 * \param[in] pTm ��������� �� ��������� ������ �������
 * \return 0 � ����� ������ ��� ��� ������.
 *		������ -ENOTSUP ��������, ���������� �� ������������ ������ ����������.
*/
int urtc_save_timestamp (const URTC_h hDev, const struct tm*__restrict pTm);

/** \brief ����� ����������� ��������� � �������������� ���������
 *
 * � ������ ��������� ���������� ������������������� ����������� �������� ���������� ��-���������.
 *
 * \param[in] hDev ��������� �� ���������� ����� ��������� ����� �� ������� TODO: ...
 * \return 0 � ����� ������ ��� ��� ������.
 *		������ -ENOTSUP ��������, ���������� �� ������������ ������ ����������.
*/
int urtc_reset_default(const URTC_h hDev);

/** ������� ���������� � ���������� �������. ������ � ��������� ���������� ���������� ������.
* ��� ��������� NULL, ���������� EINVAL
*/
int close_urtc (URTC_h __restrict hDev);

/* ������� �������� ���������� � ������� ������� ��������� �� ������.
 * ������� close_urtc ���������� �������������.
*/
void destroyRtcDevice (URTC_h hDev);


/* �������������� ������� ��� ������� � ����� I2C
 * ������� ���� ���������� �� ����� ��������������� (����), ������������ ��� ������� �������� ������ � ������ ������� �������������.
 */
chipDev_t* getChipDeviceDesc (URTC_h __restrict hDev);

 /** ������� i2c ���������� � ������� N �� /dev/i2c-N
  * \return 0 � ������ ������ ��� ��� ������; ������ ENXIO - ������������ ���� � ����������.
 */
int open_i2c_device (chipDev_t*__restrict pDev, const char*__restrict pDevPath);
int close_i2c (chipDev_t*__restrict pDev);
int i2c_read_array (const chipDev_t*__restrict pDev, uint32_t offset, uint8_t*__restrict pBuf, size_t buf_size);
int i2c_write_array (const chipDev_t*__restrict pDev, uint32_t offset, const uint8_t*__restrict pBuf, size_t buf_size);

/** ������ ������ �� 'i2c_write_array', �� ����� ������� �������� ���������� �� ��� �� �������, ����������, ��������� � ���������� ������
 * ���������� �����. �������� ��������� ������� ������, �� �������� ���������� �� ����� ������ ��� ������ � ��������� ��������� (��������� ������� � ��������).
 */
int i2c_update_array (const chipDev_t*__restrict pDev, uint32_t offset, uint8_t*__restrict pBuf, size_t buf_size);

END_C_DECLS

#endif /* !LIBURTC_H */