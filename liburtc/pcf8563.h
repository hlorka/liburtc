#ifndef PCF8563_H
#define PCF8563_H 1

#include <stdint.h>

#define PCF8563_CHIPNAME	"PCF8563"
#define PCF8563_I2C_BUS_ADDR	0x51

enum pcf8563_weekdays {
	PCF8563_SUNDAY = 0,
	PCF8563_MONDAY,
	PCF8563_TUESDAY,
	PCF8563_WEDNESDAY,
	PCF8563_THURSDAY,
	PCF8563_FRIDAY,
	PCF8563_SATURDAY
};

enum pcf8563_months {
	PCF8563_JANUARY = 1,
	PCF8563_FEBRARY,
	PCF8563_MARCH,
	PCF8563_APRIL,
	PCF8563_MAY,
	PCF8563_JUNE,
	PCF8563_JULY,
	PCF8563_AUGUST,
	PCF8563_SEPTEMBER,
	PCF8563_OCTOBER = 16,
	PCF8563_NOVEMBER,
	PCF8563_DECEMBER
};


/* Control and Status Registers set*/
#define PCF8563_REG_CTRL_STATUS_1	0
#define PCF8563_REG_CTRL_STATUS_2	0x01

#define PCF8563_BIT_TEST1			0x80
#define PCF8563_BIT_STOP			0x20
#define PCF8563_BIT_TESTC			0x08
typedef union _pcf8563_ctrls1_reg {
	struct {
		unsigned TEST1:1;
		unsigned :1;
		unsigned STOP:1;
		unsigned :1;
		unsigned TESTC:1;
		unsigned : 3;
	}		bits;
	uint8_t data;
} pcf8563_ctrls1_reg;

#define PCF8563_BIT_TI_TP			0x10
#define PCF8563_BIT_AF				0x08
#define PCF8563_BIT_TF				0x04
#define PCF8563_BIT_AIE				0x02
#define PCF8563_BIT_TIE				0x01
typedef union _pcf8563_ctrls2_reg {
	struct {
		unsigned : 3;
		unsigned TI_TP : 1;
		unsigned AF : 1;
		unsigned TF : 1;
		unsigned AIE : 1;
		unsigned TIE : 1;
	}		bits;
	uint8_t data;
} pcf8563_ctrls2_reg;


/* Time and Date Registers Set */
#define PCF8563_REG_VL_SECONDS		0x02
#define PCF8563_REG_MINUTES			0x03
#define PCF8563_REG_HOURS			0x04
#define PCF8563_REG_DAYS			0x05
#define PCF8563_REG_WEEKDAYS		0x06
#define PCF8563_REG_CENTURE_MOUNTH	0x07
#define PCF8563_REG_YEARS			0x08

#define PCF8563_BIT_VL		0x80
typedef union _pcf8563_vlsec_reg {
	struct {
		unsigned VL : 1;
		unsigned SECONDS : 7;
	}		bits;
	uint8_t data;
} pcf8563_vlsec_reg;

#define PCF8563_BIT_C		0x80
typedef union _pcf8563_centurymonth_reg {
	struct {
		unsigned C : 1;
		unsigned : 2;
		enum pcf8563_months : 5;
	}		bits;
	uint8_t data;
} pcf8563_centurymonth_reg;


/* Alarm Registers Set */
#define PCF8563_REG_MINUTE_ALM		0x09
#define PCF8563_REG_HOUR_ALM		0x0A
#define PCF8563_REG_DAY_ALM			0x0B
#define PCF8563_REG_WEEKDAY_ALM		0x0C

#define PCF8563_BIT_AE_M			0x80
typedef union _pcf8563_minute_alm_reg {
	struct {
		unsigned AE_M : 1;
		unsigned MINUTE_ALARM : 7;
	}		bits;
	uint8_t data;
} pcf8563_minute_alm_reg;

#define PCF8563_BIT_AE_H			0x80
typedef union _pcf8563_hour_alm_reg {
	struct {
		unsigned AE_H : 1;
		unsigned : 1;
		unsigned HOUR_ALARM : 6;
	}		bits;
	uint8_t data;
} pcf8563_hour_alm_reg;

#define PCF8563_BIT_AE_D			0x80
typedef union _pcf8563_day_alm_reg {
	struct {
		unsigned AE_D : 1;
		unsigned : 1;
		unsigned DAY_ALARM : 6;
	}		bits;
	uint8_t data;
} pcf8563_day_alm_reg;

#define PCF8563_BIT_AE_W			0x80
typedef union _pcf8563_weekday_alm_reg {
	struct {
		unsigned AE_W : 1;
		unsigned : 4;
		enum pcf8563_weekdays : 3;
	}		bits;
	uint8_t data;
} pcf8563_weekday_alm_reg;


/* CLCKOUT Control Register */
#define PCF8563_REG_CLKOUT_CTRL		0x0D
#define PCF8563_BIT_FE				0x80
typedef union _pcf8563_clckout_ctrl_reg {
	struct {
		unsigned FE : 1;
		unsigned : 5;
		unsigned FD : 2;
	}		bits;
	uint8_t data;
} pcf8563_clckout_ctrl_reg;


/* Timer Registers Set */
#define PCF8563_REG_TIMER			0x0F
#define PCF8563_REG_TIMER_CTRL		0x0E
#define PCF8563_BIT_TE				0x80
typedef union _pcf8563_timer_ctrl_reg {
	struct {
		unsigned TE : 1;
		unsigned : 5;
		unsigned TD : 2;
	}		bits;
	uint8_t data;
} pcf8563_timer_ctrl_reg;

#endif /* !PCF8563_H */
